#JIRA Test

### This project tests some JIRA functionality using the "A Test Project" project in Atlassian's public JIRA test instance (https://jira.atlassian.com/browse/TST)
This project contains three tests:

1. TestCreateIssue - Tests that new issues can be created
2. TestEditIssue - Tests that existing issues can be updated
3. TestSearchForIssue - Tests that existing issues can be found via JIRA’s search

### Execution
These tests can be run using the Maven `test` goal from the root directory of this project:

```console
mvn test
```

This will kick off the TestNG test suite to execute the three tests above in Chrome

Before starting the tests, you need to have a valid username and password and need to set it in /src/main/resources/application.properties.
This is also where you can modify your browser, browser version, and platform. Possible browser and platform values are listed in application.properties.
While many browsers and platforms are supported, this was only tested on Windows using Chrome.


###Attributions
This project was based off of the [Selenium2-Java-QuickStart-Archetype](https://github.com/sebarmeli/Selenium2-Java-QuickStart-Archetype)
