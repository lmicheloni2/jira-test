package com.loren.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

/**
 * Representation of a JIRA Issue when it's loaded
 */
public class IssuePage extends NavigationBar {

    @FindBy(how = How.ID, using = "key-val")
    private WebElement issueNumber;

    @FindBy(how = How.ID, using = "summary-val")
    private WebElement summaryValue;

    @FindBy(how = How.ID, using = "summary")
    private WebElement summaryField;

    @FindBy(how = How.ID, using = "edit-issue")
    private WebElement editIssueButton;

    @FindBy(how = How.ID, using = "type-val")
    private WebElement issueTypeValue;

    @FindBy(how = How.ID, using = "issuetype-field")
    private WebElement issueTypeField;

    @FindBy(how = How.ID, using = "versions-val")
    private WebElement affectsVersionsValue;

    @FindBy(how = How.ID, using = "versions-textarea")
    private WebElement affectsVersionsField;

    @FindBy(how = How.ID, using = "components-val")
    private WebElement componentsValue;

    @FindBy(how = How.ID, using = "components-textarea")
    private WebElement componentsField;

    @FindBy(how = How.ID, using = "customfield_10061-val")
    private WebElement animalField;

    @FindBy(how = How.ID, using = "security-val")
    private WebElement securityLevelField;

    @FindBy(how = How.ID, using = "assignee-val")
    private WebElement assigneeValue;

    @FindBy(how = How.ID, using = "assignee-field")
    private WebElement assigneeField;

    @FindBy(how = How.ID, using = "reporter-val")
    private WebElement reporterField;

    @FindBy(how = How.ID, using = "footer-comment-button")
    private WebElement commentButton;

    @FindBy(how = How.ID, using = "comment")
    private WebElement commentField;

    @FindBy(how = How.ID, using = "issue-comment-add-submit")
    private WebElement addCommentButton;

    @FindBy(how = How.ID, using = "issue_actions_container")
    private WebElement actionDiv;


    public IssuePage(WebDriver webDriver) {
        super(webDriver);
    }

    public String getSummary() {
        return summaryValue.getText();
    }

    public String getIssueType() {
        return issueTypeValue.getText();
    }

    public String getAffectsVersions() {
        return affectsVersionsValue.getText();
    }

    public String getComponents() {
        return componentsValue.getText();
    }

    public String getAnimal() {
        return animalField.getText();
    }

    public String getSecurityLevel() {
        return securityLevelField.getText();
    }

    public String getAssignee() {
        return assigneeValue.getText();
}

    public String getReporter() {
        return reporterField.getText();
    }

    public String getIssueNumber() {
        return issueNumber.getText();
    }


    public void enterSummary(String summary) {
        summaryValue.click();
        summaryField.clear();
        summaryField.sendKeys(summary);
        summaryField.sendKeys(Keys.ENTER);
        WebDriverWait wait = new WebDriverWait(webDriver, 10);
        wait.until(ExpectedConditions.visibilityOf(summaryValue));
    }

    public void selectIssueType(String issueType) {
        issueTypeValue.click();
        issueTypeField.clear();
        issueTypeField.sendKeys(issueType);
        issueTypeField.sendKeys(Keys.ENTER);
        issueTypeField.sendKeys(Keys.ENTER);
    }

    public void clickCommentButtonAndEnterComment(String comment) {
        commentButton.click();
        commentField.sendKeys(comment);
        addCommentButton.click();
        WebDriverWait wait = new WebDriverWait(webDriver, 10);
        wait.until(ExpectedConditions.visibilityOf(webDriver.findElement(By.className("action-body"))));
    }

    public UpdateIssuePage clickEditButton() {
        editIssueButton.click();
        return new UpdateIssuePage(webDriver);
    }

    public void selectAffectsVersions(String affectsVersions) {
        affectsVersionsValue.click();
        affectsVersionsField.clear();
        affectsVersionsField.sendKeys(affectsVersions);
        affectsVersionsField.sendKeys(Keys.ENTER);
        affectsVersionsField.sendKeys(Keys.ENTER);
    }

    public boolean isCommentTextPresent(String comment) {
        return actionDiv.getText().contains(comment);
    }
}
