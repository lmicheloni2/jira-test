package com.loren.pages;

import com.loren.util.PropertyLoader;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;

/**
 * Representation of the JIRA Dashboard page
 */
public class DashboardPage extends NavigationBar {

    @FindBy(how = How.CLASS_NAME, using = "login-link")
    private WebElement logInLink;

    @FindBy(how = How.ID, using = "username")
    private WebElement usernameField;

    @FindBy(how = How.ID, using = "password")
    private WebElement passwordField;

    @FindBy(how = How.ID, using = "loginbutton")
    private WebElement loginButton;


    public DashboardPage(WebDriver webDriver) {
        super(webDriver);
    }

    public DashboardPage logIn() {
        logInLink.click();
        usernameField.sendKeys(PropertyLoader.loadProperty("user.username"));
        passwordField.sendKeys(PropertyLoader.loadProperty("user.password"));
        loginButton.click();
        return new DashboardPage(webDriver);
    }


}
