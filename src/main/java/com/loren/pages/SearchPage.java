package com.loren.pages;

import org.openqa.selenium.*;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;


/**
 * Representation of the Search page
 */
public class SearchPage extends NavigationBar {



    @FindBy(how = How.XPATH, using = "//button[@data-id='project']")
    private WebElement projectField;

    @FindBy(how = How.ID, using = "searcher-pid-input")
    private WebElement projectSearchField;

    @FindBy(how = How.XPATH, using = "//button[@data-id='issuetype']")
    private WebElement issueTypeField;

    @FindBy(how = How.ID, using = "searcher-type-input")
    private WebElement issueSearchField;

    @FindBy(how = How.XPATH, using = "//button[@original-title='Add more fields to search with']")
    private WebElement moreButton;

    @FindBy(how = How.ID, using = "criteria-input")
    private WebElement moreSearchField;

    @FindBy(how = How.XPATH, using = "//button[@data-id='reporter']")
    private WebElement reporterButton;

    @FindBy(how = How.ID, using = "reporter-input")
    private WebElement reporterSearchField;




    public SearchPage (WebDriver webDriver) {
        super(webDriver);
    }

    public void selectProject(String project) {
        projectField.click();
        projectSearchField.sendKeys(project);
        projectSearchField.sendKeys(Keys.ENTER);
    }

    public void selectIssueType(String issueType) {
        issueTypeField.click();
        issueSearchField.sendKeys(issueType);
        issueSearchField.sendKeys(Keys.ENTER);
    }

    public void selectReporter(String reporter) {
        moreButton.click();
        moreSearchField.sendKeys("Reporter");
        moreSearchField.sendKeys(Keys.ENTER);
        try {
            if(reporterSearchField.isDisplayed()) {
               // do nothing here, we just want to open the reporter dropdown if it's not already open
            }
        } catch (NoSuchElementException e) {
            reporterButton.click();
        }
        reporterSearchField.sendKeys(reporter);
        reporterSearchField.sendKeys(Keys.ENTER);
        reporterButton.click();
    }


    public IssuePage clickIssueNumber(String linkText) {
        webDriver.findElement(By.linkText(linkText)).click();
        return new IssuePage(webDriver);
    }
}
