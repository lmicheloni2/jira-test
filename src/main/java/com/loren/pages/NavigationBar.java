package com.loren.pages;

import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;

/**
 * Representation of the Navigation bar
 */
public class NavigationBar extends AbstractPage {

    @FindBy(how = How.ID, using = "create_link")
    private WebElement createLink;

    @FindBy(how = How.ID, using = "quickSearchInput")
    private WebElement quickSearch;

    @FindBy(how = How.CLASS_NAME, using = "issue-created-key")
    private WebElement createdIssuePopUp;


    public NavigationBar(WebDriver webDriver) {
        super(webDriver);
    }

    public UpdateIssuePage clickCreateIssueButton() {
        createLink.click();
        return new UpdateIssuePage(webDriver);
    }

    public NavigationBar enterQuickSearch(String text) {
        quickSearch.sendKeys(text);
        quickSearch.sendKeys(Keys.ENTER);
        if(webDriver.getCurrentUrl().contains("jql")) {
            return new SearchPage(webDriver);
        }
        else return new IssuePage(webDriver);
    }

    public String getNewIssueNumber() {
        return createdIssuePopUp.getAttribute("data-issue-key");
    }
}
