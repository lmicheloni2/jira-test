package com.loren.pages;


import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.io.File;

/**
 * Representation of the Create Issue and Edit Issue Pages
 */
public class UpdateIssuePage extends AbstractPage {

    @FindBy(how = How.ID, using = "project-field")
    private WebElement projectField;

    @FindBy(how = How.ID, using = "issuetype-field")
    private WebElement issueTypeField;

    @FindBy(how = How.ID, using = "summary")
    private WebElement summaryField;

    @FindBy(how = How.ID, using = "security")
    private WebElement securityField;

    @FindBy(how = How.ID, using = "components-textarea")
    private WebElement componentsField;

    @FindBy(how = How.ID, using = "versions-textarea")
    private WebElement affectsVersionsField;

    @FindBy(how = How.ID, using = "assignee-field")
    private WebElement assigneeField;

    @FindBy(how = How.ID, using = "description")
    private WebElement descriptionField;

    @FindBy(how = How.XPATH, using = "//span[contains(text(),'Burger options')]/../..")
    private WebElement burgerOptionsFieldset;

    @FindBy(how = How.ID, using = "customfield_10064-1")
    private WebElement lettuceCheckboxField;

    @FindBy(how = How.ID, using = "customfield_10064-3")
    private WebElement onionCheckboxField;

    @FindBy(how = How.ID, using = "attachment_box")
    private WebElement attachmentButtonField;

    @FindBy(how = How.ID, using = "customfield_10061")
    private WebElement animalTypeField;

    @FindBy(how = How.ID, using = "customfield_10061:1")
    private WebElement animalField;

    @FindBy(how = How.ID, using = "comment")
    private WebElement commentField;

    @FindBy(how = How.ID, using = "create-issue-submit")
    private WebElement createButton;

    @FindBy(how = How.ID, using = "edit-issue-submit")
    private WebElement updateButton;



    public UpdateIssuePage(WebDriver webDriver) {
        super(webDriver);
    }

    public void selectProject(String project) {
        projectField.clear();
        projectField.sendKeys(project);
        projectField.sendKeys(Keys.ENTER);
        WebDriverWait wait = new WebDriverWait(webDriver, 10);
        wait.until(ExpectedConditions.elementToBeClickable(By.id("project-field")));
    }

    public void selectIssueType(String issueType) {
        issueTypeField.clear();
        issueTypeField.sendKeys(issueType);
        issueTypeField.sendKeys(Keys.ENTER);
        WebDriverWait wait = new WebDriverWait(webDriver, 10);
        wait.until(ExpectedConditions.elementToBeClickable(By.id("issuetype-field")));
    }

    public void enterSummary(String summary) {
        summaryField.sendKeys(summary);
    }

    public void selectSecurityLevel(String securityLevel) {
        Select select = new Select(securityField);
        select.selectByVisibleText(securityLevel);
    }

    public void selectComponent(String component) {
        componentsField.sendKeys(component);
        componentsField.sendKeys(Keys.TAB);
    }

    public void selectAffectsVersion(String affectsVersion) {
        affectsVersionsField.clear();
        affectsVersionsField.sendKeys(affectsVersion);
        affectsVersionsField.sendKeys(Keys.TAB);
    }

    public void selectAssignee(String assignee) {
        assigneeField.clear();
        assigneeField.sendKeys(assignee);
    }

    public void enterDescription(String description) {
        descriptionField.sendKeys(description);
    }

    public void checkBurgerOption(String option) {
        burgerOptionsFieldset.findElement(By.xpath("//div[@class='checkbox']/label[contains(text(),'" + option + "')]/../input")).click();
    }

    public void chooseAttachment(String fileName) {
        attachmentButtonField.sendKeys(new File("src/test/resources/" + fileName).getAbsolutePath());
        WebDriverWait wait = new WebDriverWait(webDriver, 10);
        wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//input[@name='filetoconvert']/../label[contains(text(),'" + fileName + "')]")));
    }

    public void selectAnimalType(String animalType) {
        Select select = new Select(animalTypeField);
        select.selectByVisibleText(animalType);
    }

    public void selectAnimal(String animal) {
        Select select = new Select(animalField);
        select.selectByVisibleText(animal);
    }

    public void enterComment(String comment) {
        commentField.sendKeys(comment);
    }

    public DashboardPage clickCreateButton() {
        createButton.click();
        return new DashboardPage(webDriver);
    }

    public IssuePage clickUpdateButton() {
        updateButton.click();
        return new IssuePage(webDriver);
    }


}
