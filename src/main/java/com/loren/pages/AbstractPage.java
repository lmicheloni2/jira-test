package com.loren.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.PageFactory;

/*
 * Abstract class representation of a AbstractPage in the UI. AbstractPage object pattern
 * 
 * @author Sebastiano Armeli-Battana
 */
public abstract class AbstractPage {

	protected WebDriver webDriver;

	/*
	 * Constructor injecting the WebDriver interface
	 * 
	 * @param webDriver
	 */
	public AbstractPage(WebDriver webDriver) {
		this.webDriver = webDriver;
        PageFactory.initElements(webDriver, this);
	}

	public WebDriver getWebDriver() {
		return webDriver;
	}

	public String getTitle() {
		return webDriver.getTitle();
	}

    public boolean isLinkTextPresent(String linkText) {
        return (webDriver.findElements(By.linkText(linkText))).size() > 0;
    }

}
