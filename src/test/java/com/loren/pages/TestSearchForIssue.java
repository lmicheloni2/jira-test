package com.loren.pages;

import org.testng.Assert;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;


/**
 *
 */
public class TestSearchForIssue extends TestBase {

    DashboardPage dashboardPage;
    IssuePage issuePage;
    SearchPage searchPage;
    UpdateIssuePage updateIssuePage;
    String issueNumber;
    String issueSummary = "This is a search test";

    @BeforeClass
    public void testInit() {
        // Load the page in the browser
        webDriver.get(websiteUrl);
        dashboardPage = new DashboardPage(webDriver).logIn();

        // Create issue to search for
        updateIssuePage = dashboardPage.clickCreateIssueButton();
        updateIssuePage.selectProject("A Test Project");
        updateIssuePage.selectIssueType("Bug");
        updateIssuePage.enterSummary(issueSummary);
        updateIssuePage.selectSecurityLevel("Reporter and developers");
        updateIssuePage.enterDescription("This is a test description");
        dashboardPage = updateIssuePage.clickCreateButton();
        issueNumber =  dashboardPage.getNewIssueNumber();
    }

    @Test
    public void testSearchUsingIssueNumber() {
        issuePage = (IssuePage)dashboardPage.enterQuickSearch(issueNumber);
        Assert.assertEquals(issueNumber, issuePage.getIssueNumber(), "Issue " + issueNumber + " is loaded on the page");
        Assert.assertEquals(issueSummary, issuePage.getSummary(), issueSummary + " is the issue summary");
    }

    @Test
    public void testSearchUsingIssueSummaryAndQuickSearch() throws InterruptedException {
        searchPage = (SearchPage)dashboardPage.enterQuickSearch(issueSummary);
        issuePage = searchPage.clickIssueNumber(issueNumber);
        Assert.assertEquals(issuePage.getIssueNumber(),issueNumber,"Issue "+issueNumber+" is loaded on the page");
        Assert.assertEquals(issueSummary, issuePage.getSummary(), issueSummary + " is the issue summary");
    }

    @Test
    public void testSearchUsingSearchCriteria() {
        searchPage = (SearchPage)dashboardPage.enterQuickSearch(issueSummary);
        searchPage.selectProject("A Test Project");
        searchPage.selectIssueType("Bug");
        searchPage.selectReporter("Current User");
        issuePage = searchPage.clickIssueNumber(issueNumber);
        Assert.assertEquals(issuePage.getIssueNumber(),issueNumber,"Issue "+issueNumber+" is loaded on the page");
        Assert.assertEquals(issueSummary, issuePage.getSummary(), issueSummary + " is the issue summary");
    }

}
