package com.loren.pages;

import org.openqa.selenium.support.PageFactory;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import org.testng.Assert;

import java.io.File;

/**
 *
 */
public class TestCreateIssue extends TestBase {

   	UpdateIssuePage updateIssuePage;
    DashboardPage dashboardPage;
    IssuePage issuePage;

	@BeforeClass
	public void testInit() {
		// Load the page in the browser
		webDriver.get(websiteUrl);
        dashboardPage = new DashboardPage(webDriver).logIn();
	}


    @Test
    public void testCreateIssue() throws InterruptedException {

        // Create Issue
        updateIssuePage = dashboardPage.clickCreateIssueButton();
        updateIssuePage.selectProject("A Test Project");
        updateIssuePage.selectIssueType("Bug");
        updateIssuePage.enterSummary("This is a test");
        updateIssuePage.selectSecurityLevel("Reporter and developers");
        updateIssuePage.selectComponent("Component 1");
        updateIssuePage.selectComponent("Component 2");
        updateIssuePage.selectAffectsVersion("Test Version 1");
        updateIssuePage.selectAssignee("Unassigned");
        updateIssuePage.enterDescription("This is a test description");
        updateIssuePage.checkBurgerOption("Lettuce");
        updateIssuePage.checkBurgerOption("Onion");
        updateIssuePage.chooseAttachment("testFile.xml");
        updateIssuePage.chooseAttachment("testScreenshot.png");
        updateIssuePage.selectAnimalType("Monotremes");
        updateIssuePage.selectAnimal("Platypus");
        dashboardPage = updateIssuePage.clickCreateButton();
        String issueNumber =  dashboardPage.getNewIssueNumber();
        issuePage = (IssuePage)dashboardPage.enterQuickSearch(issueNumber);

        // Verify Fields
        Assert.assertTrue(issuePage.isLinkTextPresent("A Test Project"));
        Assert.assertEquals(issuePage.getSummary(),"This is a test");
        Assert.assertEquals(issuePage.getIssueType(),"Bug");
        Assert.assertEquals(issuePage.getAffectsVersions(),"Test Version 1");
        Assert.assertEquals(issuePage.getComponents(),"Component 1, Component 2");
        Assert.assertEquals(issuePage.getAnimal(),"Monotremes - Platypus");
        Assert.assertTrue(issuePage.getSecurityLevel().contains("Reporter and developers"));
        Assert.assertEquals(issuePage.getAssignee(),"Unassigned");
        Assert.assertEquals(issuePage.getReporter(),"Loren Micheloni");
        Assert.assertTrue(issuePage.isLinkTextPresent("testFile.xml"));
        Assert.assertTrue(issuePage.isLinkTextPresent("testScreenshot.png"));

    }
}
