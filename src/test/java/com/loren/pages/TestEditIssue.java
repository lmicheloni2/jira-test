package com.loren.pages;

import org.testng.Assert;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

/**
 *
 */
public class TestEditIssue extends TestBase {

    DashboardPage dashboardPage;
    UpdateIssuePage updateIssuePage;
    IssuePage issuePage;
    String issueSummary = "This is an edit test";


    @BeforeClass
    public void testInit() {
        // Load the page in the browser
        webDriver.get(websiteUrl);
        dashboardPage = new DashboardPage(webDriver).logIn();
    }

    @BeforeMethod
    public void createIssueToEdit() {
        updateIssuePage = dashboardPage.clickCreateIssueButton();
        updateIssuePage.selectProject("A Test Project");
        updateIssuePage.selectIssueType("Bug");
        updateIssuePage.enterSummary(issueSummary);
        updateIssuePage.selectSecurityLevel("Reporter and developers");
        updateIssuePage.enterDescription("This is a test description");
        dashboardPage = updateIssuePage.clickCreateButton();
        String issueNumber =  dashboardPage.getNewIssueNumber();
        issuePage = (IssuePage)dashboardPage.enterQuickSearch(issueNumber);
    }

    @Test
    public void testEditIssueFields() {
        issuePage.enterSummary("This is an edit test - Edit 1");
        issuePage.selectIssueType("Improvement");
        issuePage.selectAffectsVersions("Version 2.0");
        issuePage.clickCommentButtonAndEnterComment("This is a comment. I am entering this while the issue is loaded");

        Assert.assertEquals(issuePage.getSummary(), "This is an edit test - Edit 1");
        Assert.assertEquals(issuePage.getIssueType(), "Improvement");
        Assert.assertEquals(issuePage.getAffectsVersions(), "Version 2.0");
        Assert.assertTrue(issuePage.isCommentTextPresent("This is a comment. I am entering this while the issue is loaded"));
    }

    @Test
    public void testEditIssuePage() {
        updateIssuePage = issuePage.clickEditButton();
        updateIssuePage.enterSummary("This is an edit test - Edit 2");
        updateIssuePage.selectIssueType("Improvement");
        updateIssuePage.selectAffectsVersion("Version 2.0");
        updateIssuePage.clickUpdateButton();

        Assert.assertEquals(issuePage.getSummary(), "This is an edit test - Edit 3");
        Assert.assertEquals(issuePage.getIssueType(), "Improvement");
        Assert.assertEquals(issuePage.getAffectsVersions(), "Version 2.0");
    }

}
